# README #

This is the repository for a yolov3 tiny retrained on subset of coco within Darknet framework with addition of TextZone detection

### What is this repository for? ###

* Quick summary: b2bot-tech/Dataset/b2bot63
* Version 1.0.0


### How do I get set up? ###

* Summary of set up:

    cfg directory includes:  
        weight of network with best results at 120000 iterations retrained on 12/02/20
        network cfg 

    data directory includes: 
        63 object list in french 
        63 object list in english

* Configuration
* Dependencies

    Information on YOLO:  https://pjreddie.com/darknet/yolo/

    @article{yolov3,
        title={YOLOv3: An Incremental Improvement},
        author={Redmon, Joseph and Farhadi, Ali},
        journal = {arXiv},
        year={2018}
        }

* Database configuration
* How to run tests
* Deployment instructions
 
    Usage withing the VPP engine from: 
    https://github.com/ezdayo/vpp

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin: francois.badaud@gmail.com
* Other community or team contact: 